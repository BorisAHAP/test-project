<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m201020_170910_create_user_table extends Migration
{
	public $table = '{{%user}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable($this->table, [
		    'id'            => $this->primaryKey(),
		    'email'         => $this->string()->notNull()->unique(),
		    'password'      => $this->string(),
		    'auth_key'      => $this->string(),
		    'confirmed'     => $this->boolean()->notNull()->defaultValue(false),
		    'role'          => $this->string()->notNull()->defaultValue('user'),
		    'first_name'    => $this->string()->notNull(),
		    'last_name'     => $this->string(),
		    'created_at'    => $this->integer(),
		    'updated_at'    => $this->integer(),
	    ]);
	
	    /**
	     * Admin
	     */
	    $this->insert($this->table, [
		    'email'      => 'admin@admin.com',
		    'password'   => Yii::$app->security->generatePasswordHash('12345678'),
		    'auth_key'   => Yii::$app->security->generateRandomString(),
		    'first_name' => 'Админ',
		    'last_name'  => 'Админович',
		    'role'       => 'admin',
		    'created_at' => time(),
		    'updated_at' => time(),
		    'confirmed'  => true,
	    ]);
	    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
