<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m201020_182809_create_kitchen_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
	        'name' => $this->string(),
	        'description' => $this->text(),
	        'created_at' => $this->integer(),
        ]);
	
	    $this->createTable('{{%ingridient}}', [
		    'id' => $this->primaryKey(),
		    'name' => $this->string(),
		    'is_hidden' => $this->smallInteger(1)
	    ]);
	    
	    $this->createTable('{{%product_ingridient}}', [
		    'product_id' => $this->integer(),
		    'ingridient_id' => $this->integer()
	    ]);
	    
	    $this->addForeignKey('fk-product_ingridient-product_id-product-id', '{{%product_ingridient}}', 'product_id', '{{%product}}', 'id','CASCADE','NO ACTION');
	    $this->addForeignKey('fk-product_ingridient-product_id-ingridient-id', '{{%product_ingridient}}', 'ingridient_id', '{{%ingridient}}', 'id','CASCADE','NO ACTION');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropForeignKey('fk-product_ingridient-product_id-product-id', '{{%product_ingridient}}');
    	$this->dropForeignKey('fk-product_ingridient-product_id-ingridient-id', '{{%product_ingridient}}');
    	
        $this->dropTable('{{%product_ingridient}}');
        $this->dropTable('{{%ingridient}}');
        $this->dropTable('{{%product}}');
    }
}
