<?php

namespace app\modules\kitchen\controllers;

use app\modules\kitchen\models\ProductIngridient;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\kitchen\models\Product;
use app\modules\kitchen\models\Ingridient;
use app\modules\kitchen\models\search\ProductSearch;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
	
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}
	
	/**
	 * Lists all Product models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ProductSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	
	/**
	 * Displays a single Product model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}
	
	/**
	 * Creates a new Product model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Product();
		$ingridients = ArrayHelper::map(Ingridient::find()->asArray()->all(), 'id', 'name');
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			$ingrs = $model->elems;
			if($model->save()){
				foreach($ingrs as $ingr){
					$prod_ingr = new ProductIngridient;
					$prod_ingr->product_id = $model->id;
					$prod_ingr->ingridient_id = $ingr;
					$prod_ingr->save();
				}
				return $this->redirect(['index']);
			}
			
		}
		
		return $this->render('_form', [
			'model'       => $model,
			'ingridients' => $ingridients,
		]);
	}
	
	/**
	 * Updates an existing Product model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->elems = ArrayHelper::getColumn($model->productIngridients, 'ingridient_id');
		
		$ingridients = ArrayHelper::map(Ingridient::find()->asArray()->all(), 'id', 'name');
		
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			$ingrs = $model->elems;
			if($model->save()){
				ProductIngridient::deleteAll(['product_id' => $model->id]);
				foreach($ingrs as $ingr){
					$prod_ingr = new ProductIngridient;
					$prod_ingr->product_id = $model->id;
					$prod_ingr->ingridient_id = $ingr;
					$prod_ingr->save();
				}
				return $this->redirect(['index']);
			}
			
		}
		
		return $this->render('_form', [
			'model' => $model,
			'ingridients' => $ingridients,
		]);
	}
	
	/**
	 * Deletes an existing Product model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		
		return $this->redirect(['index']);
	}
	
	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if(($model = Product::findOne($id)) !== null) {
			return $model;
		}
		
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
