<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\kitchen\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
	
	
	<?= $form->field($model, 'elems')->widget(Select2::classname(), [
		'data'              => $ingridients,
		'maintainOrder'     => true,
		'toggleAllSettings' => [
			'selectLabel'     => '<i class="fas fa-check-circle"></i> Выбрать все',
			'unselectLabel'   => '<i class="fas fa-times-circle"></i> Отменить все',
			'selectOptions'   => ['class' => 'text-success'],
			'unselectOptions' => ['class' => 'text-danger'],
		],
		'options'           => ['placeholder' => 'Выбирите ингридиенты', 'multiple' => true],
		'pluginOptions'     => [
			'allowClear' => true,
		],
	]); ?>
	
	<?= $form->field($model, 'image')->widget(Upload::class, [
		'url'                 => ['/storage/default/upload'],
		'uploadPath'          => 'images/products/',
		'sortable'            => true,
		'maxNumberOfFiles'    => 2,
		'showPreviewFilename' => false,
		'options'             => [
		],
	
	])->label('Image'); ?>

	<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
	
	<?php ActiveForm::end(); ?>

</div>
