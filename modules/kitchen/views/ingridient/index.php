<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\kitchen\models\search\IngridientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ingridients';
$this->params['breadcrumbs'][] = $this->title;
?>
	<div class="ingridient-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ingridient', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
		
		<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'pjax'         => true,
			'filterModel'  => $searchModel,
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
				
				'name',
				[
					'label'     => 'Скрыт ?',
					'attribute' => 'is_hidden',
					'value'     => function($model)
					{
						$hidden = $model->is_hidden == 0 ? 'Нет' : 'Да';
						$val = $model->is_hidden == 0 ? 1 : 0;
						
						return '<button data-id="' . $model->id . '" data-val="' . $val . '" class="btn btn-link js-change-hide">' . $hidden . '</button>';
					},
					'format'    => 'raw',
				],
				
				[
					'class'    => ActionColumn::class,
					'header'   => false,
					'width'    => false,
					'template' => '<div class="actions">{update}{delete}</div>',
				],
			],
		]); ?>
	
</div>

<?php $js = <<< JS
$(document).on('click','.js-change-hide',function() {
    let id = $(this).data('id')
    let val = $(this).data('val')
  $.post({
  url:'/kitchen/ingridient/change-hide',
  data:{id:id,val:val},
  success:function() {
    $.pjax.reload({ container: '#w0-pjax', timeout: false });
  }
  })
})
JS;
$this->registerJs($js)
?>
