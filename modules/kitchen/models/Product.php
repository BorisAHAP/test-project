<?php

namespace app\modules\kitchen\models;

use yii\behaviors\TimestampBehavior;
use app\modules\storage\models\StorageItem;
use app\modules\storage\behaviors\UploadBehavior;

/**
 * This is the model class for table "product".
 *
 * @property int                 $id
 * @property string|null         $name
 * @property string|null         $description
 * @property int|null            $created_at
 *
 * @property ProductIngridient[] $productIngridients
 */
class Product extends \yii\db\ActiveRecord
{
	
	public $rel;
	
	public $elems;
	
	public $image;
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'product';
	}
	
	public function behaviors()
	{
		return [
			[
				'class'              => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			],
			[
				'class'            => UploadBehavior::class,
				'multiple'         => true,
				'attribute'        => 'image',
				'uploadRelation'   => 'images',
				'filesStorage'     => 'fileStorage',
				'pathAttribute'    => 'path',
				'baseUrlAttribute' => 'base_url',
				'typeAttribute'    => 'type',
				'sizeAttribute'    => 'size',
				'nameAttribute'    => 'name',
				'orderAttribute'   => false,
			],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['name'], 'string', 'max' => 255],
			[['name', 'image', 'elems'], 'required'],
			[['description'], 'string'],
			[['created_at'], 'safe'],
		
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id'          => 'ID',
			'name'        => 'Name',
			'description' => 'Description',
			'created_at'  => 'Created At',
		];
	}
	
	/**
	 * Gets query for [[ProductIngridients]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductIngridients()
	{
		return $this->hasMany(ProductIngridient::class, ['product_id' => 'id']);
	}
	
	public function getIngridients()
	{
		return $this->hasMany(Ingridient::class, ['id' => 'ingridient_id'])
		            ->via(ProductIngridient::class, ['product_id' => 'id']);
	}
	
	public function getImages()
	{
		return $this->hasMany(StorageItem::class, ['model_id' => 'id'])->onCondition(['model_name' => $this->formName()]);
	}
	
	/**
	 * @return string
	 */
	public function getThumb()
	{
		return !empty($this->images) ? $this->images[0]->src : '/images/no-product-image.png';
	}
}
