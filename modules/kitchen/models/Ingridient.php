<?php

namespace app\modules\kitchen\models;

/**
 * This is the model class for table "ingridient".
 *
 * @property int                 $id
 * @property string|null         $name
 * @property bool|null           $is_hidden
 *
 * @property ProductIngridient[] $productIngridients
 */
class Ingridient extends \yii\db\ActiveRecord
{
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'ingridient';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['is_hidden'], 'safe'],
			[['name'], 'string', 'max' => 255],
			['name', 'required'],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id'        => 'ID',
			'name'      => 'Name',
			'is_hidden' => 'Is Hidden',
		];
	}
	
	/**
	 * Gets query for [[ProductIngridients]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductIngridients()
	{
		return $this->hasMany(ProductIngridient::class, ['ingridient_id' => 'id']);
	}
	
	public function getIngridients()
	{
		return $this->hasMany(Product::class, ['id' => 'product_id'])
		            ->via(ProductIngridient::class, ['ingridient_id' => 'id']);
	}
}
