<?php

namespace app\modules\kitchen\models;

/**
 * This is the model class for table "product_ingridient".
 *
 * @property int|null   $product_id
 * @property int|null   $ingridient_id
 *
 * @property Ingridient $ingridient
 * @property Product    $product
 */
class ProductIngridient extends \yii\db\ActiveRecord
{
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'product_ingridient';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['product_id', 'ingridient_id'], 'integer'],
			[['ingridient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingridient::class, 'targetAttribute' => ['ingridient_id' => 'id']],
			[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'product_id'    => 'Product ID',
			'ingridient_id' => 'Ingridient ID',
		];
	}
	
	/**
	 * Gets query for [[Ingridient]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getIngridient()
	{
		return $this->hasOne(Ingridient::class, ['id' => 'ingridient_id']);
	}
	
	/**
	 * Gets query for [[Product]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(Product::class, ['id' => 'product_id']);
	}
}
