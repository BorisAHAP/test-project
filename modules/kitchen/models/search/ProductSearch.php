<?php

namespace app\modules\kitchen\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\kitchen\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\modules\kitchen\models\Product`.
 */
class ProductSearch extends Product
{
	
	public $search;
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['name', 'description', 'created_at'], 'safe'],
			[['search'], 'searchValidate'],
		];
	}
	
	public function searchValidate($attr)
	{
		if(count($this->$attr) < 2) {
			$this->addError($attr, 'Пожалуйста выберите больше ингредиетов');
			
			return false;
		}
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Product::find();
		
		// add conditions that should always apply here
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);
		
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
		]);
		
		$query->andFilterWhere(['like', 'name', $this->name])
		      ->andFilterWhere(['like', 'description', $this->description])
		      ->andFilterWhere(['like', 'created_at', $this->created_at]);
		
		return $dataProvider;
	}
	
	public function searchFront($params)
	{
		$query = Product::find()
		                ->with('images')
		                ->joinWith('productIngridients')
		                ->distinct();
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);
		if(!empty($this->search)) {
			$count = count($this->search);
			$query->addSelect([
				'product.*',
				'COUNT(product_ingridient.ingridient_id) as rel',
				//'If(COUNT(product_ingridient.ingridient_id) = ' . $count . ',2,IF(COUNT(product_ingridient.ingridient_id) < ' . $count . ' AND COUNT(product_ingridient.ingridient_id) > 2,1,0)) as
				// val',
			]);
			$query->groupBy(['product.id'])
			      ->orderBy(['rel' => SORT_DESC]);
			
			$condition = ['OR'];
			foreach($this->search as $search) {
				$condition[] = ['product_ingridient.ingridient_id' => $search];
			}
			$query->andWhere($condition);
			
			$query->having(['rel' => count($this->search)]);
			$models = (clone $query)->all();
			
			if(count($models) == 0) {
				$query->orHaving(['between', 'rel', 2, count($this->search)]);
			}
			$models = (clone $query)->all();
			if(count($models) == 0) {
				$query->where(['product.id' => 0]);
			}
		}
		
		return $dataProvider;
	}
}
