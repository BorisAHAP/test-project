<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;

class LoginForm extends Model
{
	public $email;
	public $password;
	
	public function rules()
	{
		return [
			[['email', 'password'], 'required'],
			['email', 'email'],
		];
	}
	
	public function in()
	{
		if($this->validate()) {
			$user = User::findOne(['email' => $this->email]);
			if($user && Yii::$app->security->validatePassword($this->password, $user->password)) {
				return Yii::$app->user->login($user);
			}
		}
		$this->addError('email', 'Incorrect email or password');
		
		return false;
	}
}
