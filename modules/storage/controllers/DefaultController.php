<?php


namespace app\modules\storage\controllers;

use app\modules\storage\actions\DeleteAction;
use yii\web\Controller;

class DefaultController extends Controller
{
	
	public function actions()
	{
		return [
			'upload' => [
				'class'                  => 'trntv\filekit\actions\UploadAction',
				'responsePathParam'      => 'path',
				'responseBaseUrlParam'   => 'base_url',
				'responseUrlParam'       => 'url',
				'responseDeleteUrlParam' => 'delete_url',
				'responseMimeTypeParam'  => 'type',
				'responseNameParam'      => 'name',
				'responseSizeParam'      => 'size',
				'deleteRoute'            => 'delete',
				'fileStorage'            => 'fileStorage',
				'fileStorageParam'       => 'fileStorage',
				'sessionKey'             => '_uploadedFiles',
				'allowChangeFilestorage' => false,
			
			],
			'delete' => [
				'class' => DeleteAction::class,
			],
			'view'   => [
				'class' => 'trntv\filekit\actions\ViewAction',
			],
		];
	}
}
