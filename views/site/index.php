<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use app\modules\kitchen\models\search\ProductSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/**
 * @var $searchModel ProductSearch
 * @var $dataProvider ActiveDataProvider
 */
?>
<div class="site-index">
	
    <div class="jumbotron">
        <h1>Найти продукт</h1>
	    <?php $form = ActiveForm::begin(['method' => 'get', 'enableAjaxValidation' => true, 'action' => Url::toRoute(['/site/index'])]) ?>
	    <p class="lead">
	        <?= $form->field($searchModel, 'search')->widget(Select2::classname(), [
		        'data'              => $ingridients,
		        'maintainOrder'     => true,
		        'showToggleAll'     => false,
		        'toggleAllSettings' => [
			        'selectLabel'     => '<i class="fas fa-check-circle"></i> Выбрать все',
			        'unselectLabel'   => '<i class="fas fa-times-circle"></i> Отменить все',
			        'selectOptions'   => ['class' => 'text-success'],
			        'unselectOptions' => ['class' => 'text-danger'],
		        ],
		        'options'           => ['placeholder' => 'Выбирите ингридиенты', 'multiple' => true, 'value' => Yii::$app->request->get('ProductSearch')['search'] ?? []],
		        'pluginOptions'     => [
			        'allowClear'      => true,
			        'tags'            => true,
			        'tokenSeparators' => [',', ' '],
		        ],
		        'pluginEvents'      => [
		            'select2:select' => 'function() {
		                let li =$(".select2-selection__choice")
		               if(li.length > 5){
		               let i = 0
		               li.each(function(){
		               if(i === 0){
		          $(this).find(".select2-selection__choice__remove").trigger("click")
		               i++
		               }
		                     })
		               }
		              
		          	            }',
		        ],
	        ])->label(false); ?>
        </p>

        <p><button type="submit" class="btn btn-lg btn-success">Собрать</button></p>
	    <?php ActiveForm::end() ?>
    </div>

    <div class="body-content">

        <div class="row">
	          <?=
	          ListView::widget([
		          'dataProvider' => $dataProvider,
		          'summary'      => false,
		          'itemView'     => '_product',
		          'emptyText'    => 'Ничего не найдено',
	          ]);
	          ?>
        </div>

    </div>
	
</div>
