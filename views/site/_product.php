<?php

use app\modules\kitchen\models\Product;

/**
 * @var $model Product
 */

?>

<div class="col-lg-4">
	<div class="bg-light">
                <h2><?= $model->name ?></h2>

                <p><?= $model->description ?></p>
				<div>
					                <img width="200px" height="200px" src="<?= $model->thumb ?>" alt="">
				</div>
		<?php if($model->rel): ?>
			<span>Количество совпадений:<?= $model->rel ?></span>
		<?php endif; ?>
            </div>
</div>
