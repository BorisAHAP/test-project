<?php

return [
	//Sign Urls
	'login'     => '/sign/login',
	
	//Admin urls
	'dashboard' => '/admin/dashboard/index',
	'admin/ingridients' => '/kitchen/ingridient/index',
	'admin/products' => '/kitchen/product/index',
	
];
