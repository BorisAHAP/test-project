<?php

return [
	'admin'   => 'app\modules\admin\Module',
	'user'    => 'app\modules\user\Module',
	'storage' => 'app\modules\storage\Module',
	'kitchen' => 'app\modules\kitchen\Module',
	'gridview' => 'kartik\grid\Module',

];
