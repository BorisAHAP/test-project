<?php

return [
	'request'      => [
		// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
		'cookieValidationKey' => 'N_ZZxAVKSOfH0c3y7J_giz_1Vj6VApfR',
		'baseUrl'             => '',
	],
	'cache'        => [
		'class' => 'yii\caching\FileCache',
	],
	'user'         => [
		'identityClass'   => 'app\modules\user\models\User',
		'enableAutoLogin' => true,
		'loginUrl'        => '/login',
	],
	'errorHandler' => [
		'errorAction' => 'site/error',
	],
	'mailer'       => [
		'class'            => 'yii\swiftmailer\Mailer',
		// send all mails to a file by default. You have to set
		// 'useFileTransport' to false and configure a transport
		// for the mailer to send real emails.
		'useFileTransport' => true,
	],
	'log'          => [
		'traceLevel' => YII_DEBUG ? 3 : 0,
		'targets'    => [
			[
				'class'  => 'yii\log\FileTarget',
				'levels' => ['error', 'warning'],
			],
		],
	],
	'db'           => $db,
	
	'urlManager'           => [
		'enablePrettyUrl' => true,
		'showScriptName'  => false,
		'rules'           => require __DIR__ . '/urls.php',
	],
	
	'fsLocal'      => [
		'class' => 'creocoder\flysystem\LocalFilesystem',
		'path'  => '@webroot/upload',
	],
	'fileStorage'  => [
		'class'               => 'trntv\filekit\Storage',
		'filesystemComponent' => 'fsLocal',
		'baseUrl'             => '@web/upload/',
	],

];
