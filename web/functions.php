<?php

use yii\helpers\VarDumper;

function dd($var)
{
	echo '<pre>';
	VarDumper::dump($var,5,true);
	die;
}

function dump($var)
{
	echo '<pre>';
	VarDumper::dump($var);
	echo '</pre>';
}
