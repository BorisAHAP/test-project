<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\kitchen\models\Ingridient;
use app\modules\kitchen\models\search\ProductSearch;

class SiteController extends Controller
{
	
	
	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	
	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex()
	{
		$searchModel = new ProductSearch();
		
		if(Yii::$app->request->isAjax && $searchModel->load(Yii::$app->request->get())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			
			return ActiveForm::validate($searchModel);
		}
		$dataProvider = $searchModel->searchFront(Yii::$app->request->queryParams);
		$ingridients = ArrayHelper::map(Ingridient::find()->asArray()->all(), 'id', 'name');
		
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'ingridients'  => $ingridients,
		]);
	}
	
	
	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionAbout()
	{
		return $this->render('about');
	}
}
